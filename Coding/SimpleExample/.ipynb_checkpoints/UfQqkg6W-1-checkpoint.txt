* Is x the row or column? This is not clear.
* There is just one tests for the Rook. This is almost like writing 
  a compiler, generating "hello world" and declaring victory.
* Do we really need three classes? This seems like we'll have duplicate 
  effort sometime in the future, or a lot of classes if we add many types of pieces.
* Should there be some way of identifying the player or side that owns the piece
  (for example, Black or White)?
