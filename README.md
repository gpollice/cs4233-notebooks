# CS4233: Object-Oriented Analysis & Design Notebooks #

This repository contains the [Jupyter](https://jupyter.org/) notebooks for my CS4233, Object-Oriented Analysis & Sesign course. I have decided to try to provide more tangible material than just pre-recorded lectures and slides for the online offering of this class. This gives students the opportunity to interact with the course material better and also have more information that they can print out. It also makes it easier to add new material or modify existing material without completely re-recording the lecture.

## Notebook catalog

* [Index](Index.ipynb)
* [Welcome to CS4233](Welcome.ipnyb)
* Basics of analysis and design
    * [Programming Paradigms](Basics/ProgrammingParadigms.ipnyb)
    * [Object Details](Basics/Objects/ObjectDetails.ipnyb)
    * [UML Classes and Objects](Basics/UMLClassesAndObjects/UMLClassesAndObjects.ipnyb)
    * [UML Class Diagrams](Basics/UMLClassDiagrams/UMLClassDiagrams.ipnyb)
    * [Analysis Basics](Basics/Analysis/AnalysisBasics.ipnyb)
        * [Use Cases](Basics/Analysis/UseCases.ipynb)
            * [Use Case Diagrams](Basics/Analysis/UMLUseCaseDiagrams/UMLUseCaseDiagrams.ipynb)
            * [Writing Use Case Details](Basics/Analysis/WritingUseCases/WritingUseCases.ipynb)
        * [User Stories](Basics/Analysis/UserStories.ipnyb)
        * [Scenarios and BDD](Basics/Analysis/Scenarios.ipnyb)
        * [Analysis Model](Basics/Analysis/AnalysisModel.ipynb)
    * [Design Basics](Basics/Design/DesignBasics.ipynb)
    * [Software Architecutre](Basics/Design/SoftwareArchitecture.ipynb)
    
* Code: your design comes to life
    * [Coding introduction](Coding/CodeIntroduction.ipynb)
    * [A simple example](Coding/SimpleExample/FirstExample-v1.ipynb)
    * [Simple example, v2](Coding/SimpleExample/FirstExample-v2.ipynb)
    * [Version 2 evaluation](Coding/SimpleExample/FirstExample-v2-evaluation.ipynb)
    * [Simple example, v3](Coding/SimpleExample/FirstExample-v3.ipynb)
    * [Version 3 evaluation](Coding/SimpleExample/FirstExample-v3-evaluation.ipynb)
    * [Streams: Introduction](Coding/Streams/Introduction.ipynb)
    * [Java Generics](Coding/Generics/GenericsIntroduction.ipynb)
    * [Bounded Generics](Coding/Generics/BoundedGenerics.ipynb)
    
* Dependency injection
    * [Introduction to Dependency Injection](DependencyInjection/Introduction.ipynb)
    * [Version 2: Simple Constructor Injection](DependencyInjection/Version2.ipynb)
    * [Version 3: Support Different Menu Types](DependencyInjection/Version3.ipynb)
    * [Version 4: Use a Menu Factory](DependencyInjection/Version4.ipynb)
    * [Version 5: Constructor dependency injection with Guice](DependencyInjection/Version5.ipynb)
    
* Test-Driven Development
    * [Introduction to TDD](TDD/Introduction.ipynb)
    * [What is TDD?](TDD/WhatIsTDD.ipynb)
    * [The Rest of TDD](TDD/RestOfTDD.ipynb)
    * [TDD by Example: Introduction](TDD/ByExample/Introduction.ipynb)
    * [TDD by Example: Iteration 1](TDD/ByExample/Iteration1.ipynb)
    * [TDD by Example: Iteration 2](TDD/ByExample/Iteration2.ipynb)
    * [TDD by Example: Iteration 3](TDD/ByExample/Iteration3.ipynb)
    * [TDD by Example: Iteration 4](TDD/ByExample/Iteration4.ipynb)
    * [TDD by Example: Iteration 5](TDD/ByExample/Iteration5.ipynb)
    * [TDD by Example: Epilog](TDD/ByExample/Epilog.ipynb)
    * [TDD assignment: Knibble](TDD/Assignment/Knibble.ipynb)
    
* Principles
    * [Introduction to Principles](Principles/Introduction.ipynb)
    * [GoF Principles](Principles/GoFPrinciples.ipynb)
    
* Patterns
    * [Introduction to Patterns](Patterns/Introduction.ipynb)
    * Creational
        * [Creational Patterns: Factory Method](Patterns/FactoryMethod.ipynb)
            * [FactoryExamples](Patterns/FactoryExamples.ipynb)
    * Structural
    * Behavioral
    
* Escape
    * Manual
        * [Escape: Introduction](Escape/Manual/Introduction.ipynb)
        * [Escape: Game Mechanics](Escape/Manual/GameMechanics.ipynb)
        * [Escape: Game Boards](Escape/Manual/GameBoards.ipynb)
        * [Escape: Coordinates](Escape/Manual/Coordinates.ipynb)
        * [Escape: Game Pieces](Escape/Manual/GamePieces.ipynb)
        * [Escape: Game Rules](Escape/Manual/GameRules.ipynb)
        * [Escape: Configuring Games](Escape/Manual/ConfiguringGames.ipynb)
    * StartingCode
        * [Escape: Starting Code Introduction and Overview](Escape/StartingCode/Introduction.ipynb)
        * [Escape: `EscapeGameManager` Interface](Escape/StartingCode/EscapeGameManager.ipynb)
        * [Escape: Building the Game](Escape/StartingCode/BuildingGame.ipynb)
    * Releases
        * [Release 1](Escape/Releases/FirstRelease.ipynb)
        

## Attributions
* Think/TryIt icon: <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
* User story card image, https://www.scrumwithstyle.com/effective-user-stories/

