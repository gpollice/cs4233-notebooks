{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fad517f8-afc3-472a-8ba4-b2acb7c54d42",
   "metadata": {},
   "source": [
    "# Factory illustrated\n",
    "\n",
    "This notebook illustrates ways one might set up a factory object to create coordinates. Each approaches the problem in a slightly different way and there is a short discussion of the benefits and disadvantages of the approach.\n",
    "\n",
    "We start with the code from the [previous notebook](FactoryMethod.ipynb) for each example. I removed the static creation methods for simplicity.\n",
    "\n",
    "## One parameterized factory method\n",
    "\n",
    "---\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "47a453c9-f9a2-41ed-91d9-186259779fb6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%classpath add jar /usr/local/lib/notebooks/Notebooks.jar\n",
    "import static notebooks.NotebookUtilities.*;\n",
    "\n",
    "public class Piece {\n",
    "    // something useful would be in here\n",
    "}\n",
    "\n",
    "public interface Coordinate {\n",
    "    int getX();\n",
    "    int getY();\n",
    "    Piece[] getNeighbors();\n",
    "}\n",
    "\n",
    "public abstract class BaseCoordinate implements Coordinate {\n",
    "    final int x;\n",
    "    final int y;\n",
    "    \n",
    "    public BaseCoordinate(int x, int y) {\n",
    "        this.x = x;\n",
    "        this.y = y;\n",
    "    }\n",
    "    \n",
    "    public int getX() { return x; }\n",
    "    public int getY() { return y; }\n",
    "}\n",
    "\n",
    "// Hide the constructor and just provide a creation method\n",
    "public class SquareCoordinate extends BaseCoordinate implements Coordinate {\n",
    "    \n",
    "    public SquareCoordinate(int x, int y) { super(x, y); }\n",
    "    \n",
    "    public Piece[] getNeighbors() { return null; } // this would clearly be changed\n",
    "}\n",
    "\n",
    "// offers both the constructor and the creation method\n",
    "public class HexCoordinate extends BaseCoordinate implements Coordinate {\n",
    "    public HexCoordinate(int x, int y) { super(x, y); }\n",
    "    \n",
    "    public Piece[] getNeighbors() { return null; } // this would clearly be changed\n",
    "}\n",
    "\n",
    "public interface GameManager {\n",
    "    Coordinate makeCoordinate(int x, int y);\n",
    "}\n",
    "\n",
    "public enum CoordinateType {SQUARE, HEX};\n",
    "\n",
    "public class GameManagerImpl implements GameManager {\n",
    "    private CoordinateType type;\n",
    "    \n",
    "    public GameManagerImpl(CoordinateType type) { this.type = type; }\n",
    "    \n",
    "    /************** modified ****************/\n",
    "    public Coordinate makeCoordinate(int x, int y) {\n",
    "        return CoordinateFactory.makeCoordinate(type, x, y);\n",
    "    }\n",
    "}\n",
    "\n",
    "/******************* Start new ***********************/\n",
    "public class CoordinateFactory {\n",
    "    public static Coordinate makeCoordinate(CoordinateType type, int x, int y) {\n",
    "        Coordinate theCoordinate = null;\n",
    "        \n",
    "        switch (type) {\n",
    "            case SQUARE:\n",
    "                theCoordinate = new SquareCoordinate(x, y);\n",
    "                break;\n",
    "            case HEX:\n",
    "                theCoordinate = new HexCoordinate(x, y);\n",
    "                break;\n",
    "        }\n",
    "        return theCoordinate;\n",
    "    }\n",
    "}\n",
    "/******************* End new ***********************/\n",
    "\n",
    "GameManager gm = new GameManagerImpl(CoordinateType.HEX);\n",
    "Coordinate coord = gm.makeCoordinate(3, -2);\n",
    "\n",
    "// Tests\n",
    "check(coord.getX() == 3);\n",
    "check(coord.getY() == -2);\n",
    "check(coord.getClass().getName().contains(\"HexCoordinate\"));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "817bc225-3b97-42f1-9da2-a3f42d9c1105",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "In this solution the `CoordinateFactory` has just one static creation method that takes the coordinate type and calls the constructor for the particular coordinate subclass. The switch statement clearly identifies each case. This is a great choice when you simply call the constructors to create the object. We changed the `makeCoordinate()` method in the game manager to call this static method.\n",
    "\n",
    "## Multiple factory methods in the factory object\n",
    "\n",
    "This solution moved the decision as to which coordinate subclass should be created back to the game manager. It still, however, decouples the actual construction from the game manager and puts it in a factory object. It also requires the `GameManager` to instantiate an instance of the factory object rather than using static creation methods.\n",
    "\n",
    "Look closely at the way the instance is instantiated. It uses the Singleton pattern and lazy creation to provide the instance and ensure that only one instance is available during the program execution. For the simple factory object we have here, we really don't need to do lazy construction (i.e. we don't create the factory object until it is needed), but this would be the case if, for example, you needed to connect to a database when you reate the factory object.\n",
    "\n",
    "---\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a2bb92e-215e-4ab8-b614-14f442be0b77",
   "metadata": {},
   "outputs": [],
   "source": [
    "%classpath add jar /usr/local/lib/notebooks/Notebooks.jar\n",
    "import static notebooks.NotebookUtilities.*;\n",
    "\n",
    "public class Piece {\n",
    "    // something useful would be in here\n",
    "}\n",
    "\n",
    "public interface Coordinate {\n",
    "    int getX();\n",
    "    int getY();\n",
    "    Piece[] getNeighbors();\n",
    "}\n",
    "\n",
    "public abstract class BaseCoordinate implements Coordinate {\n",
    "    final int x;\n",
    "    final int y;\n",
    "    \n",
    "    public BaseCoordinate(int x, int y) {\n",
    "        this.x = x;\n",
    "        this.y = y;\n",
    "    }\n",
    "    \n",
    "    public int getX() { return x; }\n",
    "    public int getY() { return y; }\n",
    "}\n",
    "\n",
    "public class SquareCoordinate extends BaseCoordinate implements Coordinate {\n",
    "    \n",
    "    public SquareCoordinate(int x, int y) { super(x, y); }\n",
    "    \n",
    "    public Piece[] getNeighbors() { return null; } // this would clearly be changed\n",
    "}\n",
    "\n",
    "public class HexCoordinate extends BaseCoordinate implements Coordinate {\n",
    "    public HexCoordinate(int x, int y) { super(x, y); }\n",
    "    \n",
    "    public Piece[] getNeighbors() { return null; } // this would clearly be changed\n",
    "}\n",
    "\n",
    "public interface GameManager {\n",
    "    Coordinate makeCoordinate(int x, int y);\n",
    "}\n",
    "\n",
    "public enum CoordinateType {SQUARE, HEX};\n",
    "\n",
    "/******************* Start new ***********************/\n",
    "public class CoordinateFactory {\n",
    "    private static CoordinateFactory instance = null;\n",
    "    \n",
    "    private CoordinateFactory() { \n",
    "        // Intentionally empty\n",
    "    }\n",
    "    \n",
    "    public static CoordinateFactory getInstance() {\n",
    "        if (instance == null) {  // lazy instantiation\n",
    "            instance = new CoordinateFactory();\n",
    "        }\n",
    "        return instance;\n",
    "    }\n",
    "    \n",
    "    public Coordinate makeSquareCoordinate(int x, int y) {\n",
    "        return new SquareCoordinate(x, y);\n",
    "    }\n",
    "    \n",
    "    public Coordinate makeHexCoordinate(int x, int y) {\n",
    "        return new HexCoordinate(x, y);\n",
    "    }\n",
    "}\n",
    "/******************* End new ***********************/\n",
    "\n",
    "public class GameManagerImpl implements GameManager {\n",
    "    private CoordinateType type;\n",
    "    // New variable below;\n",
    "    private CoordinateFactory coordinateFactory = CoordinateFactory.getInstance();\n",
    "    \n",
    "    public GameManagerImpl(CoordinateType type) { this.type = type; }\n",
    "    \n",
    "    /************** modified ****************/\n",
    "    public Coordinate makeCoordinate(int x, int y) {\n",
    "        Coordinate theCoordinate = null;\n",
    "        switch (type) {\n",
    "            case SQUARE:\n",
    "                theCoordinate = coordinateFactory.makeSquareCoordinate(x, y);\n",
    "                break;\n",
    "            case HEX:\n",
    "                theCoordinate = coordinateFactory.makeHexCoordinate(x, y);\n",
    "                break;\n",
    "        }\n",
    "        return theCoordinate;\n",
    "    }\n",
    "}\n",
    "\n",
    "GameManager gm = new GameManagerImpl(CoordinateType.HEX);\n",
    "Coordinate coord = gm.makeCoordinate(3, -2);\n",
    "\n",
    "// Tests\n",
    "check(coord.getX() == 3);\n",
    "check(coord.getY() == -2);\n",
    "check(coord.getClass().getName().contains(\"HexCoordinate\"));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef5af04c-931d-47a1-a472-79a0f01a4b7e",
   "metadata": {},
   "source": [
    "\n",
    "This solution might be a step backwards. We have now made the `makeCoordinate()` method in the `GameManagerImpl` responsible for deciding which coordinate subclass to instantiate. We just have placed all of the creation methods in one place. While this does organize the code better, there is still a tight coupling between the `GameManagerImpl` and the different creation methods. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c1065120-a03f-40ac-8e91-5c9a34694f4e",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "Previous: [Creational Patterns: Factory Method](FactoryMethod.ipynb)  Next: [Creational Patterns: Builder](Builder.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Java",
   "language": "java",
   "name": "java"
  },
  "language_info": {
   "codemirror_mode": "java",
   "file_extension": ".jshell",
   "mimetype": "text/x-java-source",
   "name": "Java",
   "pygments_lexer": "java",
   "version": "17+35-LTS-2724"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
