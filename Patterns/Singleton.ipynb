{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8c49b9e2-e5ee-4e48-8962-dba545e659c0",
   "metadata": {},
   "source": [
    "# Creational Patterns: Singleton\n",
    "\n",
    "The Singleton pattern is one of the simplest creational patterns. Current thinking about Singleton varies. Some developers and authors do not think that it should be used while others will use it frequently.\n",
    "\n",
    "## Intent\n",
    "\n",
    "> Ensure a class only has one instance, and provide a global point of access to it.\n",
    "\n",
    "## Context\n",
    "\n",
    "Why would you want to have just one instance of a class; isn't a class a template for making lots of copies? In general the answer is yes. However, there are some objects that provide a service to a lot of other objects in your program or system, but just one will survice. The single object can maintain a state that is available to all of its clients and there is no chance of having multiple objects with conflicting states.\n",
    "\n",
    "## Example: Factories\n",
    "\n",
    "You are familiar with factories now and have seen different ways of constructing them. Let's say that you have a book factory that produces electronic books. It produces these books in PDF, mobi, and epub formats. You have a staff that takes orders for the different books and other partners who want books published. You also have a limited capacity for each type of book format and you want to make sure that you keep the production like working to an optimum advantage and avoid deadlocks and delays. This requires scheduling and tooling. It might be perfect for microservices and cloud-based systems, but you need to have a single point where any client that wants to get a book produced goes to enter the information about the book (title, ISBN) and the customer. The client might already have taken payment, but this scenario is about getting the book produced and sent to the customer as expeditiously as possible.\n",
    "\n",
    "We can set up a single `BookFactory` to manage this. We implement it as a Singleton. The UML might look like this:\n",
    "\n",
    "![](images/Singleton.png)\n",
    "\n",
    "We will simplify this example in the following code snippet and assume that the BookFactory just knows how to create the appropriate book.\n",
    "\n",
    "---\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cc03738f-d55d-490f-b237-bfc1b455d665",
   "metadata": {
    "scrolled": true,
    "tags": []
   },
   "outputs": [],
   "source": [
    "%classpath add jar /usr/local/lib/notebooks/Notebooks.jar\n",
    "import static notebooks.NotebookUtilities.*;\n",
    "\n",
    "public enum BookFormat {PDF, MOBI, EPUB};\n",
    "\n",
    "public interface Book { \n",
    "    // Just a title and format for this example\n",
    "    public String getTitle();\n",
    "    public BookFormat getFormat();\n",
    "}\n",
    "\n",
    "public class BookImpl implements Book {\n",
    "    private final String title;\n",
    "    private final BookFormat format;\n",
    "    \n",
    "    public BookImpl(String title, BookFormat format) {\n",
    "        this.title = title;\n",
    "        this.format = format;\n",
    "    }\n",
    "    \n",
    "    public String getTitle() { return title; }\n",
    "    public BookFormat getFormat() { return format; }\n",
    "}\n",
    "\n",
    "public class BookFactory {\n",
    "    // Use only static methods\n",
    "    \n",
    "    static Book produceBook(String title, BookFormat format) {\n",
    "        // This would probably be much  more complex\n",
    "        return new BookImpl(title, format);\n",
    "    }\n",
    "}\n",
    "\n",
    "// Tests\n",
    "Book book = BookFactory.produceBook(\"Debugging Teams\", BookFormat.MOBI);\n",
    "check(book.getTitle().equals(\"Debugging Teams\"));\n",
    "check(book.getFormat() == BookFormat.MOBI);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5da8da2a-0d6d-4dc2-a23c-e5ba7aa1d3ee",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "Creating the book is certainly not a simple as calling the constructor. The factory might have to decide which object can make a book of the proper type (like the other factories in the UML diagram). It may need to start up a service to create the factory or wait for a previous request to finish. There are many possibilities that will affect the performance of the factory and, as always happens with technology, things will change in the book production process. This factory provides a single place for any client that needs to have a book produces to delegat the production work.\n",
    "\n",
    "In the above example, the factory has just a static method. This might be okay for something simple like invoking a contructor of another class as we just did. Often factories or builders have a lot of complexity. Static methods just are not good enough or easy to understand when many of them make up the task. So, often, we create a factory class that gets instantiated, but only allows clients to access a single instance. See how we did this with the `CoordinateFactory` in the [Factory Illustrated notebook](FactoryExamples.ipynb).\n",
    "\n",
    "In the cell below, change the `BookFactory` to be a Singleton, but actually require the creation of the single instance. Also, create the single instance lazily; that is, don't create the instance until it is needed for the first time.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "1825f8e3-41c8-466a-927f-eccd454984ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "%classpath add jar /usr/local/lib/notebooks/Notebooks.jar\n",
    "import static notebooks.NotebookUtilities.*;\n",
    "\n",
    "public enum BookFormat {PDF, MOBI, EPUB};\n",
    "\n",
    "public interface Book { \n",
    "    // Just a title and format for this example\n",
    "    public String getTitle();\n",
    "    public BookFormat getFormat();\n",
    "}\n",
    "\n",
    "public class BookImpl implements Book {\n",
    "    private final String title;\n",
    "    private final BookFormat format;\n",
    "    \n",
    "    public BookImpl(String title, BookFormat format) {\n",
    "        this.title = title;\n",
    "        this.format = format;\n",
    "    }\n",
    "    \n",
    "    public String getTitle() { return title; }\n",
    "    public BookFormat getFormat() { return format; }\n",
    "}\n",
    "\n",
    "public class BookFactory {\n",
    "    // Have an instance that is lazily created and available to all clients.\n",
    "\n",
    "}\n",
    "\n",
    "// Tests"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f525a0f0-d8d6-4780-9c50-12fbd3ccec69",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "My solution is [here](SingletonSolution.ipynb).\n",
    "\n",
    "Previous: [Creational Patterns: Builder](Builder.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Java",
   "language": "java",
   "name": "java"
  },
  "language_info": {
   "codemirror_mode": "java",
   "file_extension": ".jshell",
   "mimetype": "text/x-java-source",
   "name": "Java",
   "pygments_lexer": "java",
   "version": "17+35-LTS-2724"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
