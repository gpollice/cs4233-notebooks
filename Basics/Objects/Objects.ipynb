{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Object Details\n",
    "Hopefully, much of the material in this notebook is just a refresher about objects and some Java. If it isn't, you should take time to make sure that you understand all of the material.\n",
    "\n",
    "## What is an *object*?\n",
    "\n",
    "Clearly, object-oriented programming (OOP) deals with objects; but what kind of object? The *New Oxford English Dictionary* has several definitions for an object. The first is the one that most people, especially those not involved with computing, might think of:\n",
    ">a material thing that can be seen and touched\n",
    "\n",
    "This is a perfectly good general definition of *object*, but it really doesn't help explain objects as we will use them. The dictionary also provides a defintion for objects in computing:\n",
    "> a data construct that provides a description of something that may be used by a computer \n",
    "> (such as a processor, a peripheral, a document, or a data set) and defines its status, \n",
    "> its method of operation, and how it interacts with other objects.\n",
    "\n",
    "[Wikipedia](https://en.wikipedia.org/wiki/Object_(computer_science) has a definition that is a little bit more focused:\n",
    "> an object can be a combination of variables, functions, and data structures\n",
    "\n",
    "If you take a few minutes to look in other sources, particularly computer science sources, you will find many other definitions of *object*, each with a slightly different description. Maybe computer scientists are like the [blind men and the elephant](https://en.wikipedia.org/wiki/Blind_men_and_an_elephant), they see objects differently, based upon their background and experiences. All of the definitions are correct for some usage of the word *object*.\n",
    "\n",
    "What are we to do with this word with so many definitions? We shall take our cue from Humpty Dumpty in Lewis Carroll's classic, *Through the Looking Glass*. “When I use a word,” Humpty Dumpty said, in rather a scornful tone, “it means just what I choose it to mean—neither more nor less.”<sup>[1]</sup>\n",
    "\n",
    "For our purposes we define an object as:\n",
    "> An entity that has three characteristics, **identity**, **state**, and **behavior**\n",
    "\n",
    "This is fairly simple, but it has everything we need to move forward, not just considering OOP, but analysis and design. We will cover each of the three characteristics in the next sections."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identity ![](Identity.png)\n",
    "\n",
    "We will start this with a little story. You just purchased a new car and you go to the dealer to pick it up. The salesperson will put dealer plates on the car and take you to the motor vehicles office where you will get your permanent ones. You go out to the lot and notice that there are several cars that look just like the one you purchased. Which is yours? The salesperson goes up to one of the cars and looks at the Vehicle Identification Number (VIN) and compares it to the number on your invoice. If the numbers do not match, you repeat the process until your car is located.\n",
    "\n",
    "In this case, the VIN would be the *identity* for your car. Within the context of finding your car, the VIN is the only identifying characteristic that guarantees that the car is the one you have purchased.\n",
    "\n",
    "After you get your license plates put on the car, you can use the license plate number to identify your car as well. This would be much easier than trying to memorize the VIN. For your context now where the vehicle has your license plate, it is the identity characteristic for your car. This is, however an identity characteristic that could change when you sell the car to someone and they replace the license plates with theirs.\n",
    "\n",
    "If you go to a hotel, restaurant, or show where there is valet parking, the valet takes your car and parks it. You do not know where the car is and when you come out you need to identify your car for the valet. If you have a ticket that the valet gave you when you arrived, you can give it to the valet and that is the identity in this context. If you were not given a ticket, you can describe the car &mdash; \"It's a purple Spark with orange racing stripes!!?\" The valet can probably pick it out of a crowd.\n",
    "\n",
    "The point here is that there may be may ways to identify an object, some more permanent than others. The appropriate choice will depend upon the context."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### State ![](State.png)\n",
    "\n",
    "Objects have a set of attributes that may vary over time. The object has a structure that is defined by a set of attributes (also called properties). Properties have values and their values at any particular point in time is the **state** of the object. In a pure object-oriented system these attributes are also objects. In some implementation languages like Java, they may be primitives that are not associated with a Java class, but this is just an implementation detail. Some languages allow the set of attributes in the object's structure to vary, while other languages require a immutable set of attributes, but the values of the attributes may change. If an object's attributes are an immutable set and the attribute values are immutable, then the object is immutable. If the attribute set can change, or at least one attribute can change its value, the the object is mutable.\n",
    "\n",
    "Formally, an object's state can be represented by a set of pairs <a,v> where **a** is the name of the attribute and **v** is the  value of the attribute. For example, if we hava a object that represents a person, and the state consists of just the first and last name, then we could represent the state of the object, **C** for Chris jones as S<sub>C</sub> = [<firstName,Chris>,<lastName,Jones>].\n",
    "\n",
    "If we were to add the age to the object's attributes we would have **C** for Chris Jones as S<sub>C</sub> = [<firstName,Chris>,<lastName,Jones>,<age,21>]. If the object has mutable state, then that same object would change on Chris's birthday to S<sub>C</sub> = [<firstName,Chris>,<lastName,Jones>,<age,22>]. If we let S<sub>C</sub> be the state the day before the birthday and S'<sub>C</sub> be the state on Chris's birthday, we can represent this by saying S'<sub>C</sub> = S<sub>C</sub>[age/22]. This indicates that S'<sub>C</sub> has the same state as S<sub>C</sub> with the exception of the **age** attribute having the new value of 22. If the state changes multiple values simultaneously, we can represent them for some object, **O** like this: \n",
    "\n",
    "<div style=\"padding-left: 1in;\">\n",
    "S'<sub>O</sub> = S<sub>O</sub>[p<sub>i</sub>/v<sub>i</sub>,p<sub>i</sub>/v<sub>i</sub>,...] \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Behavior ![](Behavior.png)\n",
    "\n",
    "The final characteristic of objects is *behavior*. Behavior defines what an object can do &mdash; that is the tasks that the object can perform! In OOP, this refers to the (public) methods implemented in the object. Objects generally perform their tasks by delegating parts of their tasks to other objects that are designed to perform that particular subtask.\n",
    "\n",
    "One way of thinking of an object-oriented application is that it is like a enterprise that creates some \n",
    "type of product. Most enterprises have an organization chart that has the company officers, directors, managers, accountants, engineers, and so on. Most of these roles get their job done by collaborating with other roles, often delegating parts of the work. At the bottom of this network are workers who perform one task well and send their output to others who requested the work.\n",
    "\n",
    "How objects get their jobs done is hidden from other objects. The public interface, if it is designed properly, describes the object's behavior and forms a contract that other objects (the clients) should expect when they delegate work to the object."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Objects for OOAD\n",
    "Not all objects are equal. When building a system based upon the O-O paradigm, we have objects that we use to model the program and describe the design. We also have objects that we create in software that perform the work as described in the specification (design). There may be some correspondence between the design objects and the implementation objects, but often they are quite different because they are based upon a different view of the system. Using OOAD techniques to evolve the design let us create cohesive components that are design objects that work together to get the job done. As the design evolves we work at finer granularities until we arrive at the individual implementation objects.\n",
    "\n",
    "### Design objects\n",
    "One of the benefits of using the O-O paradigm is that we can model real-world problems and solutions. We analyze a problem and identify the objects involved and what they do. We build a model of the system in which the problem occurs. We take the objects and identify their purpose, attributes, and behavior and begin to create a model of the objects and how they work together. At times we might remove objects that are not needed to solve the problem at hand. If we do a good job of analyzing the problem we are more likely to decompose objects into smaller collaborating objects and group them into coherent components. Design occurs at many levels. \n",
    "\n",
    "One of the keys to getting the right design objects is assigning behavior. Assigning behavior to the obects is, according to many respected O-O practitioners, the critical part of the process; some say that it's the only thing you have to do. Many techniques and tools have been developed that allow us to analyze a problem, extract the appropriate objects, and seamlessly translate them into the design objects and then into the implementation objects.\n",
    "\n",
    "Design objects are closely related to classes, or may, in fact, be classes. You will see how we discover  concepts during analysis and morph them into design objects and then implementation objects.\n",
    "\n",
    "#### Object or class\n",
    "You should understand the difference between classes and objects from previous courses. The class is a *template* or *blueprint* for creating a certain type of object.<sup>[3]</sup> There are many ways to define classes, especially at the programming language level. Objects are actual instances of the classes that have states with actual values and behavior that is executable.\n",
    "\n",
    "In the above discussion, I have used the term *object* in some places where *class* might be a better choice. Hopefully the context helps to disambiguate the usage.\n",
    "\n",
    "Previous: [Programming Paradigms](../ProgrammingParadigms.ipynb)\n",
    "Next: [UML Classes and Objects](../UMLClassesAndObjects/UMLClassesAndObjects.ipynb)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## References and further reading\n",
    "\n",
    "\\[1] Carroll, Lewis, 1832-1898. *Through The Looking-Glass and What Alice Found There*.\n",
    "\n",
    "\\[2] *Steve Jobs Tells the Best Definition of Object-Oriented Programming*, Fossbytes, https://fossbytes.com/steve-jobs-tells-the-best-definition-of-object-oriented-programming/. This is a great description that anyone should be able to understand.\n",
    "\n",
    "[2]: https://fossbytes.com/steve-jobs-tells-the-best-definition-of-object-oriented-programming/ \"Steve Jobs Tells the Best Definition of Object-Oriented Programming\"\n",
    "\n",
    "\\[3] *What is the Difference Between Classes and Objects?*, W3Schools, https://www.w3schools.in/java-questions-answers/difference-between-classes-objects/.\n",
    "\n",
    "[3]: xhttps://www.w3schools.in/java-questions-answers/difference-between-classes-objects/, \"What is the Difference between Classes and Objects\""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Java",
   "language": "java",
   "name": "java"
  },
  "language_info": {
   "codemirror_mode": "java",
   "file_extension": ".jshell",
   "mimetype": "text/x-java-source",
   "name": "java",
   "pygments_lexer": "java",
   "version": "17+35-LTS-2724"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
