{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Programming Paradigms\n",
    "While this course is called *Object-Oriented Analysis & Design*, it might be better named *Software Design: Patterns, Testing, and Techniques*. This still does not convey the full scope of the course, but it broadens the scope somewhat. Even though we will be using objected-oriented (O-O) principles for much of the course, we will be mixing this with other types of programming paradigms in order to produce software that is reliable, efficient, flexible, and maintainable.\n",
    "\n",
    "## What is a paradigm?\n",
    "I am sure that you have seen the word \"paradigm\" before and you might have used it in conversation. If not, here is the definition from the *New Oxford American Dictionary*:\n",
    "\n",
    ">a typical example or pattern of something; a model\n",
    "\n",
    "In other words, a paradigm describes a particular way of doing something. For software developers the programming paradigm describes the way to use the features of one or more programming languages to create software applications. [Wikipedia](https://en.wikipedia.org/wiki/Programming_paradigm) identifies two broad classifications for programming paradigms, *imperative* and *declarative*. \n",
    "\n",
    "Imperative languages, like **C**, require us to provide explicit instructions that cause the underlying machine to perform operations that change its state. Imperative languages may be organized into blocks of computation, like functions and subroutines, but in the end, they all instruct the machine (real or virtual) to perform a set of steps that change state. \n",
    "\n",
    "Declarative language operate on a higher level where we describe computations and the characteristics of the result. These languages typically compose programs from smaller parts. Often, the smaller parts are hidden from the programmer.\n",
    "\n",
    "Many programming paradigms exist. Some are very academic and not very useful for most software development. Let's look at the three most common programming paradigms; we will use all of these in the course, but we will use Java as the main language since it supports all of these paradigms, but we might look at some other languages that might be simpler to show in examples.\n",
    "\n",
    "## Three popular paradigms\n",
    "The three paradigms that we will concentrate on in this course are *object-oriented* (of course), *procedural*, and *functional*. As computing changes, new hardware and languages that best supports the hardware emerge. From approximately the mid-1980s to 2010 object-oriented languages and design techniques were in vogue. Prior to this period, most of the software was developed procedurally using languages like PL/1, Pascal, COBOL, C, and assembler languages. There were some declarative languages in use like Prolog, LISP, and RPG, but they were not in the majority. As multi-core hardware and distributed computation has become necessary for computational purposes, declarative paradigms (specifically functional) that work best with immutable state that is needed for parallel computation have come back into the picture.\n",
    "\n",
    "### The procedural paradigm\n",
    "We will use [Wikipedia](https://en.wikipedia.org/wiki/Procedural_programming) for our definitions again.\n",
    "\n",
    ">Procedural programming is a programming paradigm, derived from imperative programming,\n",
    ">based on the concept of the procedure call. Procedures (a type of routine or subroutine)\n",
    ">simply contain a series of computational steps to be carried out.\n",
    "\n",
    "An example of using the procedural paradigm in Java would be a method that computes the average of a sequence of integers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double proceduralAverage(int ... ints) {\n",
    "    int sum = 0;\n",
    "    for (int i : ints) { sum += i; }\n",
    "    return Double.valueOf(sum) / Double.valueOf(ints.length);\n",
    "}\n",
    "\n",
    "proceduralAverage(1, 2, 3);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since the Java engine for this document is based upon **jshell**, we can interact with the Java runtime without declaring objects and a `main()` method. When you see code like above, you can modify it and experiment with it as much as you like.\n",
    "\n",
    "Notice that the `average` method describes the procedure to follow to compute the average of a set of numbers that are presented as an array of integers. It is an example of the procedural imperative paradigm paradigm. It has mutable state (`sum`) that keeps track of the sum of the integers in the input array. The value of `sum` changes on each iteration of the for-loop. We say that this is procedural because it is not just a sequence of steps to follow, but is packaged as a function (method, procedure) that can be reused.\n",
    "\n",
    "Organizing functions and subroutines improves the maintainability of procedural programs, but these organizing methods are not enough to have maintainable, reliable large programs. Large programs developed using procedural methods and languages are more fragile and have more defects than with other paradigms and take longer to fix.\n",
    "\n",
    "### The functional paradigm\n",
    "Functional programming, according to [Wikipedia](https://en.wikipedia.org/wiki/Functional_programming) is:\n",
    "\n",
    "> A programming paradigm where programs are constructed by applying and composing functions.\n",
    "\n",
    "Functions are trees of expressions that map values to other values. Pure functional languages have no mutable state elements, but rely on expressions to create new values for given names. In practice, most functional languages have some capability to work with a mutable state, but strong warnings about using these features usually accompanies them in the language documentation. \n",
    "\n",
    "The following code calculates the average of a sequence of integers in Java using a functional approach."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import java.util.stream.IntStream;\n",
    "import java.util.OptionalDouble;\n",
    "\n",
    "double functionalAverage(int ... ints) {\n",
    "    IntStream anIntStream = Arrays.stream(ints);\n",
    "    // average is builtin for IntStream\n",
    "    // returns the average unless there is a problem, in which case\n",
    "    // throws a noSuchElementException\n",
    "    return anIntStream.average().getAsDouble();\n",
    "}\n",
    "\n",
    "functionalAverage(3, 8, 17, -2);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This version of our average computation does not say how we are computing the average. We first call a function that creates an `IntStream` from the array, then we call the `average()` method of the stream. If you look at the `IntStream` documentation in the [Java API](https://docs.oracle.com/javase/9/docs/api/index.html?overview-summary.html) you will see that `average()` returns an`OptionalDouble` and we then call its `getAsDouble()` method that returns the value or throws an exception if there is no value (something went wrong).\n",
    "\n",
    "Try changing the arguments to `functionalAverage` to an empty array of `int` and see what happens.\n",
    "\n",
    "Functional programming languages also allow functions to be first-class elements of the language. That means that you can use a function as a value and pass that value around. Java 8 added this capability, along with other functional characteristics to the Java language. Until that time one had to use Scala or Clojure to have functional capabilities in a Java Virtual Machine (JVM) language.\n",
    "\n",
    "One benefit of functional languages is that they are more amenable to formal analysis. Immutable data allows one to reason effectively using formal methods and logic. The lambda calculus is the basis for many functional programming languages.\n",
    "\n",
    "There are many ways to write an average function using functional programming concepts. If you are a little adventurous, try writing a couple of different functional versions of calculating the average of an array of integers.\n",
    "\n",
    "<!-- Place for an anecdote -->\n",
    "\n",
    "### The object-oriented paradigm\n",
    "Finally we come to the paradigm in our course name. While we will use all three of the paradigms described in this notebook, this will be the primary paradigm for several reasons:\n",
    "\n",
    "1. We can model many problems and real-world entities as objects.\n",
    "2. Object-oriented analysis and design is currently the most used approach to designing and building systems in industry.\n",
    "3. Most of the basic design patterns we will study in this course are based upon the O-O paradigm.\n",
    "4. Java is an object-oriented language with extensions for functional programming. Methods are often procedural step-by-step imperative sequences of instructions.\n",
    "5. Almost all students have had some introduction to Java before taking this course.\n",
    "\n",
    "[Wikipedia's definition](https://en.wikipedia.org/wiki/Object-oriented_programming) of object-oriented programming is:\n",
    ">OOP is based on the concept of \"objects\", which can contain data and code: \n",
    ">data in the form of fields (often known as attributes or properties), and code, \n",
    ">in the form of procedures (often known as methods).\n",
    "\n",
    "Whereas the functional paradigm combines functions, the O-O paradigm consists of communicating objects that pass messages in order to accomplish a task. Each object, if it is well designed, has a single purpose and encapsulates data and behavior in objects. Each object has (usually) mutable state that can only be modified by interaction in a well-defined manner with the encapsulating object.\n",
    "\n",
    "A possible O-O solution to the averaging problem in Java might look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "public class OOAverager {\n",
    "    public double getAverage(int ... ints) {\n",
    "        return getAverage(Arrays.stream(ints));\n",
    "    }\n",
    "    \n",
    "    public double getAverage(IntStream ints) {\n",
    "        return ints.average().getAsDouble();\n",
    "    }\n",
    "}\n",
    "\n",
    "OOAverager averager = new OOAverager();\n",
    "System.out.println(averager.getAverage(1, 2, 3, 4, 42));\n",
    "averager.getAverage(Arrays.stream(new int[] {38, -24, 32}));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the `OOAverager` class instance has no state, but it does have the behavior of getting the average of a sequence of integers. In this case, it provide two options; getting the average of an array of integers or getting the average of a stream of integers. The actual implementation uses some functional code that we have seen before. So, is this really object-oriented? Yes! The implementation is hidden from the caller (the *client*). Any instance of `OOAverager` exhibits the same behavior and, in this case, has just one purpose &mdash; to compute the average of a sequence of numbers that can be entered in two different ways. This is what we call a *cohesive* class; which we will talk about later in the course.\n",
    "\n",
    "### Which paradigm is the best?\n",
    "People, especially students, often wonder which paradigm is best so they can focus on learning that one. There is no simple answer. Each paradigm, and each programming language has characteristics that make it more appropriate for some applications than others. Additionally, since developers create the applications, the developers' background and experience contributes to how effective the paradigm and language are for the project.\n",
    "\n",
    "Here's an anecdote that may exhibit this. Several years ago I was leading a group of engineers on a new product for a company. Two members of the group were expert LISP programmers while the rest of us were more familiar with C, C++, and Pascal. The two LISP programmers were clearly more productive in the amount of work (features) they were able to implement compared to the rest of us. Their code was correct, elegant, and succinct. However, when it came time to integrate components and handle new requirements, things slowed down. The rest of the team did not speak LISP and, therefore, when they needed to integrate non-LISP code with LISP code, they just did not understand the LISP code and the functional paradigm. In this example, it would have been better to either put together a team that could work effectively with one or more paradigms and languages with which they were all competent. Today, we have more languages, like Java that have incorporated multiple paradigms.\n",
    "\n",
    "## Summary\n",
    "There are many programming paradigms; we have looked at three of the most common ones. Since our focus will be on the object-oriented pattern, we will next look at this paradigm, and objects in the next notebook.\n",
    "\n",
    "Next: [Object Details](./Objects/Objects.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## References and further reading\n",
    "\\[1] *Is functional programming more effective than object-oriented programming?*, Andrew Tai, https://blog.learningtree.com/functional-programming-object-orientated-programming/. References several studies and experiments that evaluate functional vs. O-O programming. (What is *effective*?)\n",
    "\n",
    "[1]: https://blog.learningtree.com/functional-programming-object-orientated-programming/ \"Is functional programming more effective than object-oriented programming?\"\n",
    "\n",
    "\\[2] *Google search for comparing programming paradigms and languages*, https://scholar.google.com.ec/scholar?q=programming+productivity+studies+functional+object-oriented+procedural&hl=en&as_sdt=0&as_vis=1&oi=scholart. You might find something that could lead to interesting research or an MQP topic."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Java",
   "language": "java",
   "name": "java"
  },
  "language_info": {
   "codemirror_mode": "java",
   "file_extension": ".jshell",
   "mimetype": "text/x-java-source",
   "name": "Java",
   "pygments_lexer": "java",
   "version": "17.0.5+9-LTS-191"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
