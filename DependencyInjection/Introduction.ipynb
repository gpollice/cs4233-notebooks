{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "50163c5b-c364-464c-93da-2b8eb9ca13c7",
   "metadata": {},
   "source": [
    "# Dependency Injection: Introduction\n",
    "\n",
    "In the book *Design Patterns: Elements of Reusable Object-Oriented Software*, Erich Gamma, et al., known as the Gang of Four (GoF) identified three principles:\n",
    "1. Program to an interface\n",
    "2. Prefer delegation over inheritance\n",
    "3. Encapsulate what varies\n",
    "\n",
    "We will look at these in detail throughout the course. They are part of our \"north star\" that guides us to flexible, maintainable code. In this module, we will focus on the second principle. Instead of taking a polymorphic approach, we prefer to construct classes in such a way that they delegate responsibilities as needed to other elements such as lambdas or other classes. We saw this in the examplewe worked through in the previous example. There are many ways to construct a class by providing the pieces it needs, as we did by supplying the appropriate move validator when we constructed the piece. This is called dependency injection. \n",
    "\n",
    "Dependency injection, also referred to as *inversion of control (IoC)*, is fundamental to many, if not most, software systems today. Sometimes it is explicit and implemented by using design patterns like the Factory pattern. Other times developers use a dependency injection framework. For Java, frameworks are based upon [JSR 330](https://jcp.org/en/jsr/detail?id=330). Several frameworks are available such as [Dagger](https://dagger.dev/), [Weld](http://weld.cdi-spec.org/), [Spring](https://spring.io/), [Google Guice](https://github.com/google/guice), and others. The two that seem to be most popular are Spring and Guice; both are freely available. This tutorial in this module uses Google Guice, which I think is quite a bit simpler to use than Spring, mainly due to it's focus on dependency injection rather than the much larger scope of the Spring ecosystem.\n",
    "\n",
    "You can use Guice several  ways depending upon your needs. Many tutorials and documents are available to help you understand Guice. However, depending upon your familiarity with Java, design patterns, and principles, it may be difficult to get through the jargon. I have tried to make this tutorial sufficient for my course and hopefully more easily accessible to the general audience than some of the other tutorials.\n",
    "\n",
    "The example(s) used for this tutorial are variations on a simple program that delivers menus to the user. The basic scenario is that the user will ask for a menu for the day and receive the menu that contains instructions for how to make the main course, appetizer, etc. We will not have a complete application, but work our way through some of the variations that we might encounter and how to use techniques, including dependency injection to make our application more flexible and configurable. We will go through several versions so that you can see the differences and decide which might be best for different situations.\n",
    "\n",
    "## Version 1: Starting code\n",
    "\n",
    "The following cell has the starting code. There is a `Menu` class and a `MenuBuilder` class. There is a test to show how it works. The code is formatted to keep it short."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3217a92c-b62a-4022-9f63-d8879572abb8",
   "metadata": {},
   "outputs": [],
   "source": [
    "public class Menu\n",
    "{\n",
    "    public String getMainCourse() {\n",
    "        return \"Grilled Shrimp: ...\";\n",
    "    }\n",
    "    \n",
    "    public String getSides() {\n",
    "        return \"Sauteed snow peas: ...\";\n",
    "    }\n",
    "    \n",
    "    public String getAppetizer() {\n",
    "        return \"Stuffed olives: ...\";\n",
    "    }\n",
    "    \n",
    "    public String getDessert() {\n",
    "        return \"Pecan pie: ...\";\n",
    "    }\n",
    "    \n",
    "    public String getBeverages() {\n",
    "        return \"Pink lemonade: ...\";\n",
    "    }\n",
    "}\n",
    "\n",
    "public class MenuMaker\n",
    "{\n",
    "    private final Menu theMenu;\n",
    "    \n",
    "    public MenuMaker()\n",
    "    {\n",
    "        theMenu = new Menu();\n",
    "    }\n",
    "    \n",
    "    public Menu getMenu()\n",
    "    {\n",
    "        return theMenu;\n",
    "    }\n",
    "}\n",
    "\n",
    "// Test\n",
    "Menu menu = new MenuMaker().getMenu();\n",
    "System.out.println(\"Appetizer\\n\\t\" + menu.getAppetizer());\n",
    "System.out.println(\"Main course\\n\\t\" + menu.getMainCourse());\n",
    "System.out.println(\"Sides\\n\\t\" + menu.getSides());\n",
    "System.out.println(\"Dessert\\n\\t\" + menu.getDessert());\n",
    "System.out.println(\"Beverages\\n\\t\" + menu.getBeverages());"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a80b3767-3e3b-4376-bf8f-6b56f00e0df3",
   "metadata": {},
   "source": [
    "This version corresponds to a quick prototype that a novice programmer might write in a beginning Java programming class. It simply illustrates a proof-of-concept showing that the main logic of getting a menu and printing out its contents. To change the menus, one must change the code in the `Menu` class."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "199bb54b-e772-4728-98d9-ecace7f6997b",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## References and further reading\n",
    "\n",
    "\\[1] *Design Patterns: Elements of Reusable Object-Oriented Software*, Erich Gamma, et al., Addison-Wesley, 1994. (Available electronically through the Gordon Library)\n",
    "\n",
    "\\[2] *A hands-on session with Google Guice*, Sankalp Bhatia, freeCodeCamp, https://www.freecodecamp.org/news/a-hands-on-session-with-google-guice-5f25ce588774.\n",
    "\n",
    "\\[3] *Dependency Injection*, Wikipedia, https://en.wikipedia.org/wiki/Dependency_injection.\n",
    "\n",
    "\n",
    "\\[4] *The Factory design pattern*, DZone, https://dzone.com/articles/the-factory-design-pattern.\n",
    "\n",
    "\\[5] *Google Guice Dependency Injection Example Tutorial*, JournalDev, https://www.journaldev.com/2403/google-guice-dependency-injection-example-tutorial.\n",
    "\n",
    "\\[6] *Google Guice User Guide*, https://github.com/google/guice/wiki/.\n",
    "\n",
    "\\[7] *Java: The Factory Method Pattern*, DZone, https://dzone.com/articles/java-the-factory-pattern.\n",
    "\n",
    "\\[8] *Learn Google Guice*, https://www.tutorialspoint.com/guice/index.htm."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Java",
   "language": "java",
   "name": "java"
  },
  "language_info": {
   "codemirror_mode": "java",
   "file_extension": ".jshell",
   "mimetype": "text/x-java-source",
   "name": "Java",
   "pygments_lexer": "java",
   "version": "17+35-LTS-2724"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
