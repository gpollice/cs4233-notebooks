#! /usr/bin/env ruby -w

# Read the words file on the system and create a words.txt file in the
# current directory

wordCount = 0
counter = 0
f = File.open("xRy32.txt", "w")

File.foreach("/usr/share/dict/words") do  |line| 
  counter = counter + 1
  if counter % 50 == 0
    f. puts(line.chomp)
    wordCount = wordCount + 1
  end
end
 f.close
puts "There are #{wordCount} words"